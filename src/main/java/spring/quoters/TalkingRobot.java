package spring.quoters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TalkingRobot {

    private final List<Quoter> quoterList;

    public TalkingRobot(List<Quoter> quoterList) {
        this.quoterList = quoterList;
    }

    public void talk() {
        this.quoterList.forEach(Quoter::sayQuote);
    }
}
