package spring.quoters;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class SwitchService implements Switcher {

    Map<String, String> msgQuoter;

    public SwitchService() {
        msgQuoter = new HashMap<>();
        msgQuoter.put(String.valueOf(TerminatorQuoter.class.getSimpleName()), "TerminatorQuoter");
        msgQuoter.put(String.valueOf(ChuckNorrisQuoter.class.getSimpleName()), "ChuckNorrisQuoter");
    }

    @Override
    public void serve(String message) {
        System.out.println(msgQuoter.get(message));
    }
}
