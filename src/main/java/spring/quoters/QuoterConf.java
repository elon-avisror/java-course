package spring.quoters;

import com.github.javafaker.Faker;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan
@PropertySource("classpath:quotes.properties")
public class QuoterConf {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(QuoterConf.class);
        context.getBean(TalkingRobot.class).talk();
        context.getBean(SwitchService.class).serve("Terminator.Class");
    }

    @Bean
    public Faker faker() {
        return new Faker();
    }
}
