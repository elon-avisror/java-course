package spring.quoters;

import com.github.javafaker.Faker;
import org.springframework.stereotype.Component;

@Component
public class ChuckNorrisQuoter implements Quoter {

    private final Faker faker;

    public ChuckNorrisQuoter(Faker faker) {
        this.faker = faker;
    }

    @Override
    public void sayQuote() {
        System.out.println(this.faker.chuckNorris().fact());
    }
}
