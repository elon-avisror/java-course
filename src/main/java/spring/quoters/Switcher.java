package spring.quoters;

public interface Switcher {
    void serve(String message);
}
