package spring.quoters;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class TerminatorQuoter implements Quoter {

    private List<String> messages;

    public TerminatorQuoter(@Value("${terminator}")String line)  {
        this.messages = Arrays.asList(line.split(", "));
    }

    @Override
    public void sayQuote() {
        this.messages.forEach(System.out::println);
    }
}
