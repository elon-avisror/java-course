package introduction.tasks.task1;

import lombok.Data;

// Task 1
@Data
public class Iteration {
    final static int SIZE = 10;

    private int[] intArray;
    private double[] doubleArray;;

    public Iteration() {
        this.intArray = new int[Iteration.SIZE];
        this.doubleArray = new double[Iteration.SIZE];
    }

    public void run() {
        for (int i = 0; i < Iteration.SIZE; i++) {
            this.doubleArray[i] = i*1.5;
        }
        for (int i = 0; i < doubleArray.length; i++) {
            this.intArray[i] = (int) this.doubleArray[i];
        }

        System.out.println("the doubles are:");
        for (double num : this.doubleArray) {
            System.out.println(num);
        }

        System.out.println("\nthe integers are:");
        for (int num : this.intArray) {
            if (num != 0) {
                System.out.println(num);
            }
        }
    }
}
