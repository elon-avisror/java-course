package introduction.tasks.task2;

import javax.swing.*;

// Task 2
public class Main {
    public static void main(String[] args) {
        String ans = "";
        do {
            GuessGame guessGame = new GuessGame();
            String maxNum = JOptionPane.showInputDialog("What is the maximum guess number?");
            guessGame.play(Integer.valueOf(maxNum));
            ans = JOptionPane.showInputDialog("Do you want to proceed? (y/n)");
        } while (!ans.equals("n") && !ans.equals("N"));
    }
}
