package introduction.tasks.task2;

import javax.swing.*;

public final class GuessGame {
    public static int allBestScores;
    private int bestScore;
    private int triesCnt;

    public void play(int max) {

        // computer pick a random number, that less than max
        int computerNum = (int) (Math.random() * max + 1);

        // that asks for user to guess it
        String userNum = "";

        /**
         * Computer says: to big or to small till the picked number will be guessed
         * Best result will be stored in instance variable of GuessGame class
         * Score will be calculated by formula: max/number of tries
         */
        while (true) {
            do {
                // Use JOptionPane.showInputDialog(...) for input
                userNum = JOptionPane.showInputDialog("What is yor guess?");
            } while (Integer.valueOf(userNum) > max || Integer.valueOf(userNum) < 1);

            this.triesCnt++;
            if (computerNum == Integer.valueOf(userNum)) {
                this.printBestScore(max);
                return;
            } else if (computerNum > Integer.valueOf(userNum)) {
                System.out.println("The value is bigger than you have been guessed!");
            }
            // computerNum < Integer.valueOf(userNum)
            else {
                System.out.println("The value is smaller than you have been guessed!");
            }
        }
    }

    public void printBestScore(int max) {
        if (this.bestScore == 0) {
            this.bestScore = max / this.triesCnt;
        }

        if (this.bestScore > allBestScores) {
            allBestScores = this.bestScore;
        }
        System.out.println("The best score is: " + allBestScores);
    }
}
