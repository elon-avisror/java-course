package introduction.labs.lab2.entities;

public abstract class SupremeCharacter extends Character {
    private int randHpPower;

    public SupremeCharacter(int max, int min) {
        this.randHpPower = this.getRandom(max, min);
        this.setHp(this.randHpPower);
        this.setPower(this.randHpPower);
    }

    // random hp and power between min to max
    public final int getRandom(int max, int min) {
        return (int) (Math.random()*max + min);
    }

    /**
     * decrease number of hp of the enemy by random number which will be in
     * range of his power
     * @param c is the enemy that we kick off
     */
    @Override
    public final void kick(Character c) {
        super.kick(c);
        int randPower = this.getRandom(this.getPower(), 1);
        System.out.println("DAMAGE = " + randPower);
        c.setHp(c.getHp() - randPower);
    }
}
