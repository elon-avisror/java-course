package introduction.labs.lab2.entities;

import lombok.Data;

@Data
public abstract class Character {
    private int hp;
    private int power;
    private int id;

    public void kick(Character c) {
        System.out.println(this.getCharacterDetails() + " kicks " + c.getCharacterDetails());
    }

    public String getCharacterDetails() {
        return "Character " + this.getId() + " (" + this.getClass().getSimpleName() + ")";
    }

    public final boolean isAlive() {
        return this.hp > 0;
    }

    public Character setId(int id) {
        this.id = id;
        return this;
    }
}
