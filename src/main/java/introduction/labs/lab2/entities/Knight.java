package introduction.labs.lab2.entities;

public final class Knight extends SupremeCharacter {
    private static final int MAX = 12;
    private static final int MIN = 2;

    public Knight() {
        super(MAX, MIN);
    }
}
