package introduction.labs.lab2.entities;

public final class Elf extends Character {
    private static final int HP = 10;
    private static final int POWER = 10;

    public Elf() {
        this.setHp(HP);
        this.setPower(POWER);
    }

    /**
     * kill everybody which weaker than him, otherwise decrease power of other
     * character by 1
     * @param c is the enemy that we kick off
     */
    @Override
    public final void kick(Character c) {
        super.kick(c);
        if (c.getPower() < this.getPower()) {
            System.out.println(this.getClass().getSimpleName() + " is powerful than " + c.getClass().getSimpleName() + " so goodbye...");
            System.out.println("DAMAGE = " + c.getHp());
            c.setHp(0);
        }
        else {
            System.out.println(this.getClass().getSimpleName() + " equal to he's " + c.getClass().getSimpleName() + " so...");
            System.out.println("DAMAGE = " + 1);
            c.setPower(c.getPower() - 1);
        }
    }
}
