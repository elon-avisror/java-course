package introduction.labs.lab2.entities;

public final class Hobbit extends Character {
    private static final int POWER = 0;
    private static final int HP = 3;

    public Hobbit() {
        this.setHp(HP);
        this.setPower(POWER);
    }

    @Override
    public final void kick(Character c) {
        System.out.println(this.toCry(c));
        System.out.println("DAMAGE = 0");
    }

    public String toCry(Character c) {
        return this.getClass().getSimpleName() + " crying for mercy to " + c.getClass().getSimpleName();
    }
}
