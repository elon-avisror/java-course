package introduction.labs.lab2.entities;

public final class King extends SupremeCharacter {
    private static final int MAX = 15;
    private static final int MIN = 5;

    public King() {
        super(MAX, MIN);
    }
}
