package introduction.labs.lab2;

import introduction.labs.lab2.game.Game;
import introduction.labs.lab2.game.Heroes;

import javax.swing.*;
import java.rmi.UnexpectedException;

// Lab 2
public class Main {
    public static void main(String[] args) throws UnexpectedException, InterruptedException {
        boolean wantToPlay = true;
        do {
            Game heroesGame = new Heroes();
            heroesGame.start();

            String ans = "";
            while (!ans.equals("y") && !ans.equals("Y") && !ans.equals("n") && !ans.equals("N")) {
                ans = JOptionPane.showInputDialog("Do you want a new game?! (y/n)");
            }

            if (ans.equals("y") || ans.equals("Y")) {
                wantToPlay = true;
            }
            // ans == n/N
            else {
                wantToPlay = false;
            }

        } while (wantToPlay);

        // Game Manager
        // void fight(Character c1, Character c2)
        // to provide fight between to characters and explain via command line what happens during the fight, till both
        // of the characters are alive
    }
}
