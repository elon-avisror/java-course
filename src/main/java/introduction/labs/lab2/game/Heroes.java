package introduction.labs.lab2.game;

import introduction.labs.lab2.entities.*;
import introduction.labs.lab2.entities.Character;

import java.rmi.UnexpectedException;
import java.util.concurrent.TimeUnit;

public class Heroes extends Game {
    private static final int MAX = 4;
    private static final int MIN = 1;
    private static final String ROUND_MARK = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
    private static final String SCORE_MARK = "-------------------------------------------------------";
    private static final int SECONDS_TO_WAIT = 3;
    private Character c1;
    private Character c2;
    private int round;

    /**
     * prepare characters with a random creation function
     */
    @Override
    protected void prepareGame() throws UnexpectedException, InterruptedException {
        System.out.println("Preparing game by randomize two characters...");
        this.c1 = this.createCharacter().setId(1);
        this.c2 = this.createCharacter().setId(2);
        System.out.println(this.c1.getCharacterDetails());
        System.out.println(this.c2.getCharacterDetails());

        // Thread.sleep() is better!
        TimeUnit.SECONDS.sleep(SECONDS_TO_WAIT);
    }

    /**
     * if someone is dead
     *
     * @return if c1.hp or c2.hp is equal or lower than zero
     */
    @Override
    protected boolean endOfGame() {
        return !this.c1.isAlive() || !this.c2.isAlive();
    }

    /**
     * fight method
     */
    @Override
    protected void play() throws InterruptedException {
        System.out.println(this.getRoundDetails());
        TimeUnit.SECONDS.sleep(SECONDS_TO_WAIT);
        this.c1.kick(this.c2);
        TimeUnit.SECONDS.sleep(SECONDS_TO_WAIT);
        this.c2.kick(this.c1);
        TimeUnit.SECONDS.sleep(SECONDS_TO_WAIT);
    }

    @Override
    protected void calcScore() {
        System.out.println(this.getScore());
    }

    /**
     * Character Factory
     * Character createCharacter()
     * returns random instance of any existing character
     * @return a random Character (Hobbit, Elf, Knight or King)
     * @throws UnexpectedException
     */
    private Character createCharacter() throws UnexpectedException {
        int randNum = (int) (Math.random() * MAX + MIN);
        switch (randNum) {
            case 1:
                return new Hobbit();
            case 2:
                return new Elf();
            case 3:
                return new Knight();
            case 4:
                return new King();
            default:
                throw new UnexpectedException("The function of creating a random character was failed!");
        }
    }

    private String getRoundDetails() {
        String roundDetails = ROUND_MARK + "\n";
        roundDetails += "Round " + ++this.round + "\n" + this.getHeader(this.c1) + this.getHeader(this.c2);
        roundDetails += ROUND_MARK;
        return roundDetails;
    }

    private String getHeader(Character c) {
        return c.getCharacterDetails() + "\nHP = " + c.getHp() + "\nPOWER = " + c.getPower() + "\n";
    }

    private String getScore() {
        String gameScore = SCORE_MARK + "\nSCORE\n";
        gameScore += this.getWinner();
        gameScore += SCORE_MARK;
        return gameScore;
    }

    private String getWinner() {
        if (this.c1.getHp() == this.c2.getHp()) {
            return "It's a Draw!\n";
        } else if (this.c1.getHp() > 0) {
            return this.c1.getCharacterDetails() + " Wins!\n";
        }
        // this.c2.getHp() > 0
        else {
            return this.c2.getCharacterDetails() + " Wins!\n";
        }
    }
}
