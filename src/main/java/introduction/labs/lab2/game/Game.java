package introduction.labs.lab2.game;

import java.rmi.UnexpectedException;

public abstract class Game {
    public final void start() throws UnexpectedException, InterruptedException {
        this.prepareGame();
        while (!this.endOfGame()) {
            this.play();
        }
        this.calcScore();
    }

    protected abstract void prepareGame() throws UnexpectedException, InterruptedException;
    protected abstract boolean endOfGame();
    protected abstract void play() throws InterruptedException;
    protected abstract void calcScore();
}
