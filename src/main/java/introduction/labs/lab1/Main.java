package introduction.labs.lab1;

// Lab 1
public class Main {
    public static void main(String[] args) {
        Employee.Builder builder = new Employee.Builder();
        Employee moshe = builder.age(30).bonus(20).salary(30).name("Moshe").debt(0).build();
        System.out.println(moshe);
    }
}
