package introduction.labs.lab1;

import lombok.Data;
import lombok.NonNull;
import lombok.ToString;

// Lab 1
@Data
@ToString
public class Employee {
    @NonNull
    private final String name;
    private final int age;
    private final int salary;
    @NonNull
    private final int bonus;
    private final int debt;

    private Employee(String name, int age, int salary, int bonus, int debt) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.bonus = bonus;
        this.debt = debt;
    }

    public static class Builder {
        private String name;
        private Integer age;
        private Integer salary;
        private Integer bonus;
        private Integer debt;

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder age(int age) {
            this.age = age;
            return this;
        }

        public Builder salary(int salary) {
            this.salary = salary;
            return this;
        }

        public Builder bonus(int bonus) {
            this.bonus = bonus;
            return this;
        }

        public Builder debt(int debt) {
            this.debt = debt;
            return this;
        }

        public Employee build() {
            this.validate();
            Employee employee = new Employee(this.name, this.age, this.salary, this.bonus, this.debt);
            this.cleanBuilder();
            return employee;
        }

        // we can create a new one instead of cleaning it
        private void cleanBuilder() {
            this.name = "";
            this.age = 0;
            this.salary = 0;
            this.bonus = 0;
            this.debt = 0;
            // ...
        }

        // validate interface (for each filed)
        private void validate() {
            if (this.name == null) {
                throw new IllegalStateException("name can't be null!");
            }
            if (this.bonus == null) {
                throw new IllegalStateException("bonus need's to be specified!");
            }
        }
    }
}
