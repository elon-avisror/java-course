package introduction.labs.lab3;

import java.util.List;

// Lab 3
public class Delay {
    public static <T> void forEachWithDelay(List<T> list, int delay, Delayer<T> delayer) throws InterruptedException {
        for (T t : list) {
            Thread.sleep(delay);
            delayer.makeDelay(t);
        }
    }
}
