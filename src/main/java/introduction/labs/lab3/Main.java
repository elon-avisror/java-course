package introduction.labs.lab3;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        int delay = 10;
        ArrayList<String> list = new ArrayList<String>();

        list.add("hii");
        list.add("my");
        list.add("name");
        list.add("is");
        list.add("java");

        Delay.forEachWithDelay(list, 3000, System.out::println);
    }
}
