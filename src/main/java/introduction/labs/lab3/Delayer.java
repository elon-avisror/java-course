package introduction.labs.lab3;

public interface Delayer<T> {
    void makeDelay(T t);
}
