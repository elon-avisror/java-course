package java_8.labs.lab2;

public class BenchmarkCleaner implements Cleaner {

    @InjectByType
    CleanerImpl cleaner;

    public void clean() {
        long startTime = System.nanoTime();
        cleaner.clean();
        long endTime = System.nanoTime();
        System.out.println("duration: " + (endTime - startTime));
    }
}
