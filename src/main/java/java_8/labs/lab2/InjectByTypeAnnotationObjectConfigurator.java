package java_8.labs.lab2;

import lombok.SneakyThrows;

import java.lang.reflect.Field;
import java.util.Random;

// Lab 2
public class InjectByTypeAnnotationObjectConfigurator implements ObjectConfigurator {
    @Override
    @SneakyThrows
    public void configure(Object t) {
        Class<?> type = t.getClass();
        Field[] fields = type.getDeclaredFields();
        for (Field field : fields) {
            InjectRandomInt annotation = field.getAnnotation(InjectRandomInt.class);
            if (annotation != null) {
                int min = annotation.min();
                int max = annotation.max();
                Random random = new Random();
                int value = random.nextInt(max - min) + min + 1;
                field.setAccessible(true);
                field.set(t, value);
            }
        }
    }
}
