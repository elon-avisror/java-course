package java_8.labs.lab2;

// Lab 2
public interface Config {
    <T> Class<T> getImplClass(Class<T> type);
}
