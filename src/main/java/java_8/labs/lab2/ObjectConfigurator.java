package java_8.labs.lab2;

// Lab 2
public interface ObjectConfigurator {
    void configure(Object t);
}
