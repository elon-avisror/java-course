package java_8.labs.lab2;

import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

// Lab 2
@Retention(RUNTIME)
public @interface InjectRandomInt {
    int min();
    int max();
}
