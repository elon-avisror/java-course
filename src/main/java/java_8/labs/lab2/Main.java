package java_8.labs.lab2;

// Lab 2
public class Main {
    public static void main(String[] args) {
        // Every time when method clean is working you need to print to console
        // how many time it took.
        // Prohibited to change any existing classes...
        // Allowed:
        // 1. Add new Classes
        // 2. Change JavaConfig file
        // In order to check you have succeeded,
        // just run the main method from Main class
        IRobot iRobot = ObjectFactory.getInstance().createObject(IRobot.class);
        iRobot.cleanRoom();
    }
}
