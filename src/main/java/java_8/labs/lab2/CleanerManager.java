package java_8.labs.lab2;

public class CleanerManager implements Cleaner {

    @InjectByType
    CleanerImpl cleaner;

    public void clean() {
        long startTime = System.nanoTime();
        cleaner.clean();
        long endTime = System.nanoTime();
        System.out.println(endTime - startTime);
    }
}
