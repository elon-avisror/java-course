package java_8.labs.lab2;

import java.util.HashMap;
import java.util.Map;

// Lab 2
public class JavaConfig implements Config {
    private Map<Class, Class> ifc2Class = new HashMap<>();

    public JavaConfig() {
        ifc2Class.put(Speaker.class, ConsoleSpeaker.class);
        ifc2Class.put(Cleaner.class, BenchmarkCleaner.class);
    }

    @Override
    public <T> Class<T> getImplClass(Class<T> type) {
        return ifc2Class.get(type);
    }
}
