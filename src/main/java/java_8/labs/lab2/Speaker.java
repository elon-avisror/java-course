package java_8.labs.lab2;

public interface Speaker {
    void speak(String message);
}
