package java_8.labs.lab1;

import introduction.labs.lab1.Employee;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

public class Main {
    public static void main(String[] args) throws IOException {
        /*
        Employee elon = new Employee.Builder().name("Elon").salary(10).bonus(10).build();
        Employee tehila = new Employee.Builder().name("Tehila").salary(20).bonus(30).build();
        int asInt = Stream.of(elon, tehila)
                .mapToInt(Employee::getSalary)
                .reduce(Integer::sum)
                .getAsInt();

        System.out.println(asInt);
        Map<String, Long> collect = Arrays.stream(words)
                .map(String::toLowerCase)
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        System.out.println(collect);
        */

        SongYesterday songYesterday = new SongYesterday();
        String songWords = songYesterday.getSongWords();
        String[] words = songWords.split(" ");

        // count words in song yesterday
        long countWords = Arrays.stream(words)
                .mapToInt(String::length)
                .count();
        System.out.println("The number of words in song yesterday is: " + countWords);

        // find most popular word in song yesterday (you will never guess)
        String mostPopularWord = Arrays.stream(words)
                .map(String::toLowerCase)
                .max(String::compareTo)
                .get();

        System.out.println("The most popular word in the song yesterday is: " + '"' + mostPopularWord + '"');

        // define enum seniority (JUNIOR(0,10), MIDDLE(11,20), SENIOR(21,Long.MAX_VALUE))
        // write method in DeveloperUtil which will receive list of Developers and will return
        // Map<Seniority,Long> - seniority against number of developers with that seniority
        // * do not touch class Developer
        // * do not use any if in your code.

        List<Beer> beers = Arrays.asList(new Beer("Tuborg", 10), new Beer("Carlsberg", 12), new Beer("Weihnestephan", 14));
        List<Developer> developers = Arrays.asList(
                new Developer(10, beers),
                new Developer(20, beers),
                new Developer(30, beers),
                new Developer(9, beers),
                new Developer(7, beers),
                new Developer(1, beers),
                new Developer(17, beers));
        System.out.println(DeveloperUtils.totalSeniorityDevelopers(developers));
    }
}
