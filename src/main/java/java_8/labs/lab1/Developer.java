package java_8.labs.lab1;

import lombok.*;

import java.util.List;

// Lab 1
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Developer {
    private int salary;
    @Singular
    private List<Beer> beers;
}
