package java_8.labs.lab1;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

// Lab 1
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Beer {
    private String name;
    private int price;
}
