package java_8.labs.lab1;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DeveloperUtils {
    public static int totalSalary(List<Developer> developers) {
        return developers.stream().mapToInt(Developer::getSalary).sum();
    }

    public static int totalBeerCost(List<Developer> list) {
        return list.stream()
//                .parallel()
                .map(Developer::getBeers)
                .flatMap(List::stream)
//                .sequential()
                .mapToInt(Beer::getPrice)
                .sum();
//                .flatMap((Developer developer) -> developer.getBeers().stream())
    }

    public static Map<Seniority, Long> totalSeniorityDevelopers(List<Developer> developers) {
        return developers.stream()
                .mapToLong(Developer::getSalary)
                .mapToObj(Seniority::findSeniorityBySalary)
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
    }
}
