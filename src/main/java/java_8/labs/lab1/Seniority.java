package java_8.labs.lab1;

import lombok.Getter;
import lombok.SneakyThrows;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// Lab 1
@Getter
public enum Seniority {
    JUNIOR("Junior", 0, 10),
    MIDDLE("Middle", 11, 20),
    SENIOR("Senior", 21, Long.MAX_VALUE);

    private final String name;
    private final long bottomSalary;
    private final long topSalary;

    Seniority(String name, long bottomSalary, long topSalary) {
        this.name = name;
        this.bottomSalary = bottomSalary;
        this.topSalary = topSalary;
    }

    @SneakyThrows
    public static Seniority findSeniorityBySalary(long salary) {
        Seniority[] seniorities = values();
        return Arrays.stream(seniorities)
                .filter(seniority -> seniority.getBottomSalary() <= salary)
                .filter(seniority -> salary <= seniority.getTopSalary())
                .findFirst()
                .get(); // orElseThrow()
    }
}
